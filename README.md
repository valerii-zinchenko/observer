# Observer

Simple observer to work with events. It can be used as a stand-alone observer or can be mixed in in some other object/function/class to directly reuse the functionality of an observer there.

All attached event handlers are stored in FIFO order. They are stored as is, i.e. if binding to some context is required then it should be done manually.


## Requirements

It is written on vanilla JavaScript :)

It does not require any additional libraries.


## Installation

```
$ npm install @valerii-zinchenko/observer --save
```

Available library files:

- `dest/observer.js` - not minified library
- `dest/observer.min.js` - minified library

The destination library files are surrounded with the [universal module definition](https://github.com/umdjs/umd/). So it can be loaded

- as a module for NodeJS
- as an AMD module
- or will be stored into the global variable under the name `Observer`


## Usage

Simple event with property

```js
// start listen 'hi' event by constructing a new instance
var observer = new Observer('hi', function(name) {
	console.log('hi ' + name);
});

// start listen 'hi' event with a new handler function
observer.listen('hi', function(name) {
	console.log('hi ' + name + ' again');
});


observer.trigger('hi', 'world');
// The followwing lines will be logged in the console.
// 'hi world'
// 'hi world again'
```

Example of encapsulating of Observer functionality into a class, by using a [class-wrapper](https://github.com/valerii-zinchenko/class-wrapper).

```js
// Define classes
// --------------------
// Human class, that can call a cat and receive an answer
var Human = Class(function() {
	// listen an "answer" event, i.e. an answer from someone
	this.listen('answer', this.onAnswer);
}, {
	Encapsulate: Observer,

	// call a cat
	callCat: function(cat) {
		cat.trigger('call', this);
	},

	// handle an answer from a cat
	onAnswer: function(answer) {
		console.log(answer);
	}
});

// Cat class, that listens any calls and answers to the caller
var Cat = Class(function() {
	// listen "call" event, i.e. someone will call a cat
	this.listen('call', this.onCall);
}, {
	Encapsulate: Observer,
	
	// handle a "call" event
	onCall: function(initiator) {
		// answer to an initiator
		initiator.trigger('answer', 'Meow! :P');
	}
});
// --------------------


// Create instances
// --------------------
var human = new Human();
var cat = new Cat();
// --------------------


// some human calls some cat
human.callCat(cat);
// The following line will be logged into the console
// 'Meow! :P'
```


## Links

- [wiki](https://gitlab.com/valerii-zinchenko/observer/wikis/home)
- [API](https://valerii-zinchenko.gitlab.io/observer/doc/nightly/index.html)
- [Run unit tests](https://valerii-zinchenko.gitlab.io/observer/test/index.html)
