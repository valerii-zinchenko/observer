/* global Observer:false */
/* eslint-disable no-magic-numbers, max-nested-callbacks, require-jsdoc */

// eslint-disable-next-line
'use strict';

describe('Observer', function() {
	describe('1. Class', function() {
		it('1. It should be able to behave like a stand-alone class', function() {
			assert.isObject(Observer.prototype._events, '1. Observer\'s prototype should have own default storage of events');
		});

		describe('2. Stand-alone class', function() {
			it('1. Exception should be thrown if an observer will be called without `new` operator', function() {
				assert.throws(function() {
					// eslint-disable-next-line new-cap
					Observer();
				}, Error, '"new" operator is required');
			});

			it('2. Always a new instance should be returned', function() {
				var inst;
				var inst2;

				assert.doesNotThrow(function() {
					inst = new Observer();
					inst2 = new Observer();
				});

				assert.notEqual(inst, inst2);
			});

			it('3. A new instance should not use the default storage', function() {
				var inst;

				assert.doesNotThrow(function() {
					inst = new Observer();
				});

				assert.notEqual(Observer.prototype._events, inst._events);
			});

			it('4. Each instance should have own storage of events', function() {
				var inst;
				var inst2;

				assert.doesNotThrow(function() {
					inst = new Observer();
					inst2 = new Observer();
				});

				assert.notEqual(inst._events, inst2._events);
			});
		});
	});

	describe('2. Instance', function() {
		var instance;
		beforeEach(function() {
			instance = new Observer();
		});
		afterEach(function() {
			instance = null;
		});

		describe('1. Attaching handlers to an event', function() {
			var fns = [];
			for (var n = 0; n < 2; n++) {
				fns.push(function() {});
			}

			[
				{
					title: '1. It should be able to accept an event name and simple function',
					input: ['event name', fns[0]],
					expected: {
						'event name': [fns[0]]
					}
				},
				{
					title: '2. It should be able to accept an object, where the property name will be interpreted as an event name and property value - as handler function of that event',
					input: [{
						'event name': fns[0],
						'event name 2': fns[0]
					}],
					expected: {
						'event name': [fns[0]],
						'event name 2': [fns[0]]
					}
				}
			].forEach(function(testCase) {
				it(testCase.title, function() {
					var eventName = 'event';
					function fn() {}

					var expected = {};
					expected[eventName] = [fn];

					assert.deepEqual(instance._events, {}, 'For some reason the storage is not empty for a test');
					assert.doesNotThrow(function() {
						instance.listen.apply(instance, testCase.input);
					});
					assert.deepEqual(instance._events, testCase.expected, 'Storage of events should contain anything else');
				});
			});
		});

		describe('2. Remove a listener', function() {
			function fn() {}
			beforeEach(function() {
				instance.listen('event', fn);
			});

			it('1. It should be able to remove an exact handler (by reference)', function() {
				assert.deepEqual(instance._events, {
					event: [fn]
				}, 'For some reason storage hasn\'t any registered events');
				assert.doesNotThrow(function() {
					instance.removeListener('event', fn);
				});
				assert.deepEqual(instance._events, {});
			});

			describe('2. It should not throw any exception if:', function() {
				[
					{
						title: '- an event, from which a handler is going to be removed, is not found',
						input: ['unregistered event', fn]
					},
					{
						title: '- a handler, which is going to be removed, is not found',
						input: ['event', function() {}]
					}
				].forEach(function(testCase) {
					it(testCase.title, function() {
						assert.deepEqual(instance._events, {
							event: [fn]
						}, 'For some reason storage hasn\'t any registered events');
						assert.doesNotThrow(function() {
							instance.removeListener.apply(instance, testCase.input);
						});
						assert.deepEqual(instance._events, {
							event: [fn]
						});
					});
				});
			});
		});

		describe('3. Trigger an event', function() {
			describe('1. It should be able to trigger any event and it should not throw any exception if:', function() {
				it('- there is no registered event', function() {
					assert.doesNotThrow(function() {
						instance.trigger('not registered event');
					});
				});

				it('- there is no registered handlers', function() {
					instance._events['event without handlers'] = [];

					assert.doesNotThrow(function() {
						instance.trigger('event without handlers');
					});
				});
			});

			it('2. All registered event handlers should be executed in the same order as they were attached (FIFO queue)', function() {
				var fns = [sinon.spy(), sinon.spy(), sinon.spy()];

				assert.doesNotThrow(function() {
					instance.listen('event', fns[0]);
					instance.listen('event', fns[2]);
					instance.listen('event', fns[1]);

					instance.trigger('event');
				});

				assert.isFalse(fns.some(function(fn) {
					return !fn.calledOnce;
				}), 'Some function was not called once');

				assert.isTrue(fns[2].calledAfter(fns[0])
					&& fns[1].calledAfter(fns[2])
					&& fns[1].calledAfter(fns[0]));
			});


			it('3. It should be able to trigger an event with additional arguments', function() {
				var fns = [sinon.spy(), sinon.spy()];
				var arg0 = 2;
				var arg1 = {};
				var arg2 = false;
				// eslint-disable-next-line no-undef-init
				var arg3 = undefined;
				var arg4 = null;
				// eslint-disable-next-line func-style
				var arg5 = function() {};
				var arg6 = '';
				var arg7 = [];

				assert.doesNotThrow(function() {
					instance.listen('event', fns[0]);
					instance.listen('event', fns[1]);

					instance.trigger('event', arg0, arg1, arg2, arg3, arg4, arg5, arg6, arg7);
				});

				assert.isTrue(fns[0].calledWithExactly(arg0, arg1, arg2, arg3, arg4, arg5, arg6, arg7));
				assert.isTrue(fns[1].calledWithExactly(arg0, arg1, arg2, arg3, arg4, arg5, arg6, arg7));
			});

			it('4. An exception by executing of some event handler function should not break an execution of other attached handlers. The exception error should be printed in the console', function() {
				var fns = [sinon.spy(), sinon.stub().throws(), sinon.spy()];
				sinon.spy(console, 'warn');

				assert.doesNotThrow(function() {
					instance.listen('event', fns[0]);
					instance.listen('event', fns[1]);
					instance.listen('event', fns[2]);

					instance.trigger('event');
				});

				assert.isFalse(fns.some(function(fn) {
					return !fn.calledOnce;
				}), 'Some of the registered functions was not called');
				// eslint-disable-next-line no-console
				assert.isTrue(console.warn.calledWithExactly('An error was thrown for the event "event" in the handler #1:', fns[1].exceptions[0]));
			});
		});
	});
});
