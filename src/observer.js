/*
 * Observer
 *
 * Copyright (c) 2016-2018  Valerii Zinchenko
 * Licensed under MIT (https://gitlab.com/valerii-zinchenko/observer/blob/master/LICENSE.txt).
 * All source files are available at: http://gitlab.com/valerii-zinchenko/observer
 */

/**
 * @file Implementation of an [observer]{@link Observer}.
 *
 * @author Valerii Zinchenko
 */

// eslint-disable-next-line
'use strict';

/**
 * Observer.
 *
 * All input arguments are directly passed into [listen]{@link Observer#listen} method.
 *
 * @class
 *
 * @param {String | Object} [eventName] - Event name. If this argument is object then the keys it will be treated as event names and key values as event handlers
 * @param {Function} [handler] - Event handler
 *
 * @throws {Error} "new" operator is required
 */
function Observer(eventName, handler) {
	if (!this) {
		throw new Error('"new" operator is required');
	}

	// create own storage of events
	this._events = {};

	this.listen.apply(this, arguments);
}

/**
 * Holder of registered events
 *
 * @private
 * @type {Object}
 */
Observer.prototype._events = {};

/**
 * Start listening a specific event(s) and attach an event handler(s) to them.
 *
 * @param {String | Object} eventName - Event name. If this argument is object then the keys it will be treated as event names and key values as event handlers
 * @param {Function} [handler] - Event handler
 */
Observer.prototype.listen = function(eventName, handler) {
	switch (Object.prototype.toString.call(eventName)) {
		case '[object Object]':
			var obj = eventName;
			// eslint-disable-next-line no-param-reassign
			for (eventName in obj) {
				this.listen(eventName, obj[eventName]);
			}
			break;

		case '[object String]':
			if (!this._events[eventName]) {
				this._events[eventName] = [];
			}
			this._events[eventName].push(handler);
			break;

		// no default
	}
};

/**
 * Trigger specific event.
 *
 * The event handler function will be called with all arguments that come after "eventName" argument.
 *
 * @param {String} eventName - Event name that should be triggered
 * @param [...rest] - Arguments that will be applied to the registered event handlers
 */
Observer.prototype.trigger = function(eventName) {
	var events = this._events[eventName];
	if (!events || events.length === 0) {
		return;
	}

	var args = Array.prototype.slice.call(arguments, 1);
	for (var n = 0, N = events.length; n < N; n++) {
		try {
			events[n].apply(this, args);
		} catch (err) {
			// eslint-disable-next-line no-console
			console.warn('An error was thrown for the event "' + eventName + '" in the handler #' + n + ':', err);
		}
	}
};

/**
 * Remove registered event handlers.
 *
 * @param {String} eventName - Event name from where the event handler will be removed
 * @param {Function} handlerRef - Original reference to the registered event handler
 */
Observer.prototype.removeListener = function(eventName, handlerRef) {
	var events = this._events[eventName];
	if (!events) {
		return;
	}

	for (var n = 0, N = events.length; n < N; n++) {
		if (events[n] === handlerRef) {
			events.splice(n,1);
			break;
		}
	}

	// remove an event name from the registry if there is no more handlers of it
	if (events.length === 0) {
		delete this._events[eventName];
	}
};
