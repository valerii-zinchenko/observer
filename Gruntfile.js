module.exports = function(grunt) {
	require('load-grunt-tasks')(grunt);

	var src = 'src/';

	var banner = '// <%= pkg.description %>\n'+
				 '// v<%= pkg.version %>\n' +
				 '// Copyright (c) 2016-2018 <%= pkg.author %>\n' +
				 '// License: <%= pkg.license %> http://valerii-zinchenko.gitlab.io/observer/blob/master/LICENSE.txt\n' +
				 '// All source files are available at: <%= pkg.homepage %>\n';

	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),

		umd: {
			pkg: {
				options: {
					src: src + '*.js',
					dest: '<%= pkg.directories.dest %>/observer.js',
					template: 'umd.hbs',
					objectToExport: 'Observer'
				}
			}
		},

		uglify: {
			options: {
				banner: banner
			},
			dist: {
				files: {
					'<%= pkg.directories.dest %>/observer.min.js': '<%= pkg.directories.dest %>/observer.js'
				}
			}
		},

		copy: {
			test: {
				expand: true,
				cwd: './node_modules',
				src: [
					'mocha/mocha.js',
					'mocha/mocha.css',
					'chai/chai.js',
					'sinon/pkg/sinon.js'
				],
				dest: 'test/lib',
				flatten: true
			}
		},

		jsdoc: {
			options: {
				configure: 'jsdoc.conf.json',
			},

			/*
			doc: {
				src: src + '*.js',
				options: {
					package: "package.json",
				}
			},
			*/

			nightly: {
				src: src + '*.js',
				options: {
					destination: 'doc/nightly'
				}
			}
		},

		eslint: {
			options: {
				configFile: 'eslint.conf.js',
				envs: [
					'browser',
					'node',
					'commonjs',
					'amd',
					'mocha'
				],
				globals: ['assert', 'sinon']
			},
			target: [src + '*.js', '<%= pkg.directories.test %>/*.js', '!<%= pkg.directories.test %>/polifills.js']
		}
	});


	[
		['build', ['umd', 'uglify']]
	].forEach(function(registry){
		grunt.registerTask.apply(grunt, registry);
	});
};
